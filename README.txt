Hardware
========
Plug a switch between GPIO 14 and GND.
[Typically, pins 8 & 6.]

Full Software Installation
==========================
-Install Raspberry Pi OS (10/2021: used version 2021-05-07)
-Enable sshd
-Disable ssh password authentication
-Change default password!!!
-Install the following packages: 'screen', 'raspi-gpio', 'htop', 'unattended-upgrades'
-Configure 'unattended-upgrades' as you wish
-Put 'kfetdoor.sh' in some useful location
-In the same directory, create a file 'TOKEN' containing 'TOKEN=<token to send>',
  which wll be appended to status updates requests.
-`echo ':msg,contains,"gpiomem device opened" ~' > /etc/rsyslog.d/01-kfetdoor.conf`
-Set-up the cron using 'crontab'

