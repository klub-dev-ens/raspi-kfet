#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

. $DIR/TOKEN

# Set up GPIO 14 as input in pullup mode
# This is where the switch should be plugged
# (and the other pin should go to GND)
raspi-gpio set 14 ip pu

# Last read door state
OLDSTATE=0
# Timestamp of last <send> event to server
OLDSTATEDATE=0

while true; do
	# Get the current door state
	# Door states: 0=open 1=closed
	STATE=`raspi-gpio get 14 | cut -f 3 -d' ' | cut -f 2 -d'='`
	STATEDATE=`date +%s`
	# We send the current state to the server if it change or if we have not done it for a while
	if [[ ("$OLDSTATE" -ne "$STATE") || ($STATEDATE -ge $(( $OLDSTATEDATE + 120 )) ) ]]
	then
		#Debug line, does not hurt anyone
		echo "State: $OLDSTATE -> $STATE | Time-delta: $(( $STATEDATE - $OLDSTATEDATE ))"
		OPENTEXT="raw_open=$STATE"
		curl -sS --max-time 20 -L "https://cof.ens.fr/k-fet/open/raw_open"  --data "token=$TOKEN" --data "$OPENTEXT"
		OLDSTATE=$STATE
		OLDSTATEDATE=$STATEDATE
	fi
	# No need to poll the switch state all the time
	sleep 1
done
